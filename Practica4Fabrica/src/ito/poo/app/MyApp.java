package ito.poo.app;

import java.time.LocalDate;

import ito.poo.clases.Prendas;
import ito.poo.clases.lote;


public class MyApp {
	
	static void run() {

		lote lt;
		
		lt= new lote();
		
		
		System.out.println(lt);
		
		LocalDate fecha=LocalDate.now();
		 
		System.out.println(new lote(2, null, null));
		System.out.println(new lote(2, null, fecha));
		System.out.println(new lote(2, "seis", null));
		System.out.println(new lote(2, "diez", fecha));
		
		Prendas pd;
		
		pd= new Prendas();
		System.out.println(pd);
		System.out.println(new Prendas(03, null, 1f, null, null, null));
		System.out.println(new Prendas(10, "algodon", 125f, "Masculino", "Verano", null));
	}


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		run();

	}

}
